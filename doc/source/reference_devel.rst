Reference of low level classes and functions
============================================

Decoding
--------

Temporal Decoding
_____________________________

.. currentmodule:: obob_mne.decoding
.. autosummary::
    :toctree: generated/

    TemporalArray

Generalized Temporal Decoding
_____________________________

.. currentmodule:: obob_mne.decoding
.. autosummary::
    :toctree: generated/

    GeneralizedTemporalArray
    GeneralizedTemporalFromCollection
