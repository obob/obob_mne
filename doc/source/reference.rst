Reference
=========

Raw Files
---------

.. currentmodule:: obob_mne.mixins.raw
.. autosummary::
    :toctree: generated/
    :template: class.rst

    LoadFromSinuhe
    AdvancedEvents
    AutomaticBinaryEvents
    AutomaticBinaryEventsWithMetadata

Decoding
--------

Temporal Decoding
_____________________________

.. currentmodule:: obob_mne.decoding
.. autosummary::
    :toctree: generated/
    :template: class.rst

    Temporal


Generalized Temporal Decoding
_____________________________

.. currentmodule:: obob_mne.decoding
.. autosummary::
    :toctree: generated/
    :template: class.rst

    GeneralizedTemporal
    GeneralizedTemporalAverage
    GeneralizedTemporalStatistics

