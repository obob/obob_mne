# simple makefile to simplify repetetive build env management tasks under posix

# caution: testing won't work on windows, see README

VERSION_FILE:=VERSION
VERSION:=$(strip $(shell cat ${VERSION_FILE}))
PYPIVERSION:=$(subst _,.,$(VERSION))
pypidist:=dist/obob_mne-$(PYPIVERSION).tar.gz

ifeq ($(findstring dev,$(VERSION)), dev)
	export TWINE_REPOSITORY_URL=https://test.pypi.org/legacy/
	ifeq ($(shell echo -n $(PYPIVERSION) | tail -c 1), v)
		PYPIVERSION:=$(PYPIVERSION)0
	endif
	ISDEV:=1
else
	ISDEV:=0
endif

flake:
	@if command -v flake8 > /dev/null; then \
		echo "Running flake8"; \
		flake8 --count obob_mne; \
	else \
		echo "flake8 not found, please install it!"; \
		exit 1; \
	fi;
	@echo "flake8 passed"

pydocstyle:
	@echo "Running pydocstyle"
	@pydocstyle obob_mne

build-doc:
	cd doc; make clean; make html

autobuild-doc:
	sphinx-autobuild doc/source doc/build

clean-dist:
	conda-build purge-all
	rm -rf dist
	rm -rf obob_mne.egg-info

$(pypidist):
	python setup.py sdist

make-dist: $(pypidist)

upload-dist: make-dist
	twine upload --config-file .pypirc dist/obob_mne-$(PYPIVERSION).tar.gz
ifneq ($(findstring dev,$(VERSION)), dev)
	git tag -a v$(VERSION) -m "version $(VERSION)"
	git push origin v$(VERSION)
endif

pep: flake pydocstyle

clean-doc:
	cd doc; make clean; rm -rf source/generated

